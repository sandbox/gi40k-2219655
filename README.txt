Description
-----------

This module turns your Drupal site into a website monitoring agent that sends
you email alerts if any of the monitored sites become unresponsive.

You will be able to register any number of URL addresses with the module,
and it will regularly check to verify if the target sites are online. Three
methods are available to determine whether a site is online:

- Status check: Sends a request to the target site, and checks the HTTP status
  code returned.
- Text match: Requests a page from the target site, and looks for a text in
  the returned body text.
- Regex match: Requests a page from the target site, and performs a regex match
  on the returned body text.


Dependencies
------------

The module requires the Entity API module.


Installation
------------

- Install Entity API module.

- Install module using the standard installation method.

- Ensure that cron runs frequently enough. Alerts are triggered on cron runs,
  so it needs to run as frequently as the smallest check frequency you are
  planning on using. E.g. if you would like a target site to be checked every
  15 minutes, cron needs to run every 15 minutes.
  Note that Drupal's built-in cron does not allow cron frequencies less than
  every hour. If you need to run cron more frequently, you will need to set
  up a server cron job.
  Take a minute to consider what each cron run might do on your Drupal site.
  It might not be feasible to run cron every 15 minutes if your site does very
  resource intensive tasks during cron runs.


Usage
-----

- Go to Configuration>Site monitors.
- Click 'Add site monitor' to register a site for monitoring.
- Watch your inbox for alerts.
