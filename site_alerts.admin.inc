<?php

/**
 * @file
 * Site Alerts admin functions.
 */

/**
 * Form callback for the site monitor edit form. Called by entity.module.
 */
function site_alerts_monitor_form($form, &$form_state, $site_monitor = NULL) {
  $form = array();

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => (!empty($site_monitor->name) ? $site_monitor->name : ''),
    '#description' => t('The name of the site monitor.'),
    '#required' => TRUE,
  );

  $form['address'] = array(
    '#type' => 'textfield',
    '#title' => t('Address'),
    '#default_value' => (!empty($site_monitor->address) ? $site_monitor->address : ''),
    '#description' => t('The url address of the website this monitor checks.'),
    '#required' => TRUE,
  );

  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Check type'),
    '#options' => array(
      SITE_ALERTS_MONITOR_TEXT_MATCH => t('Text match'),
      SITE_ALERTS_MONITOR_REGEX_MATCH => t('Regex match'),
      SITE_ALERTS_MONITOR_STATUS_CHECK => t('Status check'),
    ),
    '#default_value' => (!empty($site_monitor->type) ? $site_monitor->type : SITE_ALERTS_MONITOR_TEXT_MATCH),
    '#description' => t('The type of check used to determine if the monitored site is up.'),
    '#required' => TRUE,
  );

  $form['search_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Search text'),
    '#default_value' => (!empty($site_monitor->search_text) ? $site_monitor->search_text : ''),
    '#description' => t('Search text used for status verification. If the check type is "Text match", specify a text that appears on the monitored web page; for "Regex match", specify a regular expression that matches content on the monitored web page. Not used for "Status check" type of monitors.'),
    '#states' => array(
      'invisible' => array(
        ':input[name="type"]' => array('value' => SITE_ALERTS_MONITOR_STATUS_CHECK),
      ),
      'optional' => array(
        ':input[name="type"]' => array('value' => SITE_ALERTS_MONITOR_STATUS_CHECK),
      ),
    ),
  );

  $form['recipient'] = array(
    '#type' => 'textfield',
    '#title' => t('Alert recipient'),
    '#default_value' => (!empty($site_monitor->recipient) ? $site_monitor->recipient : ''),
    '#description' => t('The email address to which alert emails should be sent. If left empty, alerts will be sent to the site email.'),
  );

  $form['frequency'] = array(
    '#type' => 'select',
    '#title' => t('Check frequency'),
    '#options' => array(
      300 => t('5 minutes'),
      900 => t('15 minutes'),
      1800 => t('30 minutes'),
      3600 => t('60 minutes'),
    ),
    '#default_value' => (!empty($site_monitor->frequency) ? $site_monitor->frequency : 900),
    '#description' => t('Specifies how often the site status will be checked. Status checks depend on cron, so cron needs to run at least as frequently as the status checks.'),
    '#required' => TRUE,
  );

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
    ),
  );

  return $form;
}

/**
 * Validation handler for the site monitor edit form.
 */
function site_alerts_monitor_form_validate($form, &$form_state) {
  if (!valid_url($form_state['values']['address'])) {
    form_set_error('address', t('Please enter a valid url.'));
  }

  if (!empty($form_state['values']['recipient']) && !valid_email_address($form_state['values']['recipient'])) {
    form_set_error('recipient', t('Please enter a valid email address.'));
  }

  if (($form_state['values']['type'] == SITE_ALERTS_MONITOR_TEXT_MATCH || $form_state['values']['type'] == SITE_ALERTS_MONITOR_REGEX_MATCH) && empty($form_state['values']['search_text'])) {
    form_set_error('search_text', t('You need to enter a search text for text and regex checks.'));
  }
}

/**
 * Submit handler for the site monitor edit form.
 */
function site_alerts_monitor_form_submit($form, &$form_state) {
  // This entity api function creates the entity based on the submitted
  // form values.
  $entity = entity_ui_form_submit_build_entity($form, $form_state);
  $entity->save();
  drupal_set_message(t('The site monitor has been saved.'));
  $form_state['redirect'] = 'admin/config/services/site-alerts';
}
