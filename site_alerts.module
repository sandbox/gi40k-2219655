<?php

/**
 * @file
 * Site Alerts module file.
 */

define('SITE_ALERTS_MONITOR_TEXT_MATCH', 1);
define('SITE_ALERTS_MONITOR_REGEX_MATCH', 2);
define('SITE_ALERTS_MONITOR_STATUS_CHECK', 3);

/**
 * Implements hook_permission().
 */
function site_alerts_permission() {
  return array(
    'administer site alerts' => array(
      'title' => t('Administer site alerts'),
    ),
  );
}

/**
 * Implements hook_entity_info().
 */
function site_alerts_entity_info() {
  $info = array();

  $info['site_alerts_monitor'] = array(
    'label' => t('Site monitor'),
    'base table' => 'site_alerts_monitor',
    'entity keys' => array(
      'id' => 'mid',
      'label' => 'name',
    ),
    'entity class' => 'Entity',
    'controller class' => 'EntityAPIController',
    'admin ui' => array(
      'path' => 'admin/config/services/site-alerts',
      'menu wildcard' => '%site_alerts_monitor',
      'file' => 'site_alerts.admin.inc',
    ),
    'module' => 'site_alerts',
    'access callback' => 'site_alerts_monitor_access',
  );

  return $info;
}

/**
 * Access callback for site monitor entity.
 */
function site_alerts_monitor_access($op, $entity = NULL, $account = NULL) {
  if (user_access('administer site alerts', $account)) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Loads a site monitor entity.
 *
 * @param int $mid
 *   Site monitor entity id.
 *
 * @return int
 *   A Site monitor entity.
 */
function site_alerts_monitor_load($mid) {
  $entity = entity_load('site_alerts_monitor', array($mid));
  return array_pop($entity);
}

/**
 * Implements hook_cron().
 */
function site_alerts_cron() {
  site_alerts_trigger_alerts();
}

/**
 * Checks all sites, and triggers alerts as needed.
 */
function site_alerts_trigger_alerts() {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'site_alerts_monitor');
  $result = $query->execute();

  // Result is an array keyed by entity type.
  if (isset($result['site_alerts_monitor'])) {
    $ids = array_keys($result['site_alerts_monitor']);
    $entities = entity_load('site_alerts_monitor', $ids);
  }

  if (empty($entities)) {
    return;
  }

  foreach ($entities as $site_alerts_monitor) {

    if (REQUEST_TIME - $site_alerts_monitor->last_checked < $site_alerts_monitor->frequency) {
      continue;
    }

    $alive = site_alerts_is_alive($site_alerts_monitor);

    if (!$alive) {
      // Send alert email. Skip sending if the site was already known to be
      // down. The alert email has already been sent in that case.
      if ($site_alerts_monitor->status) {
        $params = array(
          'entity' => $site_alerts_monitor,
        );

        if (!empty($site_alerts_monitor->recipient)) {
          $recipient = $site_alerts_monitor->recipient;
        }
        else {
          $recipient = variable_get('site_mail', '');
        }

        drupal_mail('site_alerts', 'site_offline', $recipient, language_default(), $params);

        watchdog('site_alerts', 'Site @name offline.', array('@name' => $site_alerts_monitor->name), WATCHDOG_WARNING);
      }
    }

    $site_alerts_monitor->last_checked = REQUEST_TIME;

    if ($alive) {
      $site_alerts_monitor->status = 1;
    }
    else {
      $site_alerts_monitor->status = 0;
    }

    entity_save('site_alerts_monitor', $site_alerts_monitor);

  }
}

/**
 * Sends a request to the server to determine if it's alive.
 *
 * @param object $site_alerts_monitor
 *   A Site monitor entity.
 *
 * @return bool
 *   The site's status as boolean. TRUE if alive, FALSE if not.
 */
function site_alerts_is_alive($site_alerts_monitor) {

  // Address may or may not contain the protocol. Add if necessary.
  if (!preg_match('#^https?://#i', $site_alerts_monitor->address)) {
    $address = 'http://' . $site_alerts_monitor->address;
  }
  else {
    $address = $site_alerts_monitor->address;
  }

  $response = drupal_http_request($address);

  switch ($site_alerts_monitor->type) {
    case SITE_ALERTS_MONITOR_STATUS_CHECK:
      // Only need to check the http response code.
      if ($response->code == 200) {
        return TRUE;
      }
      else {
        return FALSE;
      }

    case SITE_ALERTS_MONITOR_TEXT_MATCH:
      // Check the response code, and search for the text in the returned
      // content.
      if ($response->code == 200 && strpos($response->data, $site_alerts_monitor->search_text) !== FALSE) {
        return TRUE;
      }
      else {
        return FALSE;
      }

    case SITE_ALERTS_MONITOR_REGEX_MATCH:
      // Check the response code, and do a regex search in the returned content.
      if ($response->code == 200 && preg_match('#' . $site_alerts_monitor->search_text . '#', $response->data)) {
        return TRUE;
      }
      else {
        return FALSE;
      }

    default:
      // Invalid type value. Should not happen.
      return FALSE;
  }

}

/**
 * Implements hook_mail().
 */
function site_alerts_mail($key, &$message, $params) {

  if ($key == 'site_offline') {

    $message['subject'] = t('Site alerts - @site offline', array('@site' => $params['entity']->name));

    $message['body'][] = t('The following site alert has been triggered:');
    $message['body'][] = '';
    $message['body'][] = t('Name:') . ' ' . $params['entity']->name;
    $message['body'][] = t('Address:') . ' ' . $params['entity']->address;
    $message['body'][] = t('Date:') . ' ' . format_date(REQUEST_TIME);

  }
}
